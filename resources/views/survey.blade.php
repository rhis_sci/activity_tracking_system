@extends('index')
@section('content')
    <section class="content">
        <div class="row">
            <div class="col-sm-12">
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">Facility Assessment Survey</h3>
                        <div class="box-tools">
                            <div class="btn-group pull-right" style="margin-right: 10px">
                                <a class="btn btn-sm btn-default form-history-back"><i class="fa fa-arrow-left"></i>&nbsp;Back</a>
                            </div>
                        </div>
                    </div>
                    {{--<form class="form-horizontal" method="POST" action="{{url('/survey/submit')}}" >--}}
                        {{--{{ csrf_field() }}--}}
                        <div class="form-group row geo-code" >
                            <div class="col-sm-12">
                                <div class="row">
                                    <div class="form-group col-md-3">
                                        <label for="zilla_id" class="control-label col-sm-2">District</label>
                                        <div class="col-sm-12">
                                            <input type="hidden" name="zilla_id"/>
                                            <div class="input-group">
                                                <select class="form-control" id="zilla_id" name="zilla_id">
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-3 ">
                                        <label for="upazilla_id" class="control-label col-sm-2">Upazilla</label>
                                        <div class="col-sm-12">
                                            <input type="hidden" name="upazilla_id"/>
                                            <div class="input-group">
                                                <select class="form-control " name="upazilla_id" id="upazilla_id">
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-3 ">
                                        <label for="union_id" class="control-label col-sm-2">Union</label>
                                        <div class="col-sm-12">
                                            <input type="hidden" name="union_id"/>
                                            <div class="input-group">
                                                <select class="form-control " name="union_id" id="union_id">
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-3 ">
                                        <label for="union_id" class="control-label col-sm-12">Facility Name</label>
                                        <div class="col-sm-12">
                                            <input type="hidden" name="facility_id"/>
                                            <div class="input-group">
                                                <select class="form-control " name="facility_id" id="facility_id">
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="facility-info" class="alert alert-success" style="background-color:#cce5ff!important;color: black!important;margin: 5px!important; display: none;height: 250px!important;" >
                            </br><strong>Facility Category :</strong> <span class="facility-category"></span>
                            </br><strong>District :</strong> <span class="district-name"></span>
                            </br><strong>Upazilla :</strong> <span class="upazila-name"></span>
                            </br><strong>Union :</strong> <span class="union-name"></span>
                            </br><strong>Facility Name :</strong> <span class="facility-name"></span>
                            </br><strong>Score :</strong> <span class="facility-score"></span>
                        </div>
                        <div class="box-body">
                            <div class="fields-group">
                                @foreach($sections as $section)
                                    <div class="row ">
                                        <div class="col-sm-12">
                                            <div class="panel panel-info section-panel">
                                                <div class="panel-heading panel-heading-section">
                                                    <h3 class="panel-title">
                                                        <span class="clickable"><i class="btn-plus fa fa-minus"></i></span>
                                                        {{$section->section_name}} &nbsp;&nbsp;({{$section->details}})
                                                    </h3>
                                                    <span class="pull-right clickable"><i class="btn-arrow fa fa-chevron-up"></i></span>
                                                </div>
                                                <div class="panel-body">
                                                    @foreach($section->questions as $question)
                                                        <div class="single-question row">
                                                            <div class="survey-question col-sm-10">
                                                                <div class="question-title">{{$section->section_code}}.{{$loop->iteration}}&nbsp;&nbsp;{{$question->question_text}}</div>
                                                                <div class="question-body {{$question->question_id}}">
                                                                    @foreach($question->options as $option)
                                                                        <div class="option-block ">
                                                                            @if($question->question_type == 'multiple')
                                                                                <input type="checkbox" name="feedback[{{$section->section_id}}_{{$question->question_id}}_{{$option->option_id}}]" value="{{$option->option_id}}" id="{{$option->option_id}}" class="{{$option->disabled_question_ids}}">
                                                                                <label class="control-label" for="{{$option->option_id}}">&nbsp;{{$option->option_text}}</label>
                                                                                <div class="form-check-inline" style="display: none">
                                                                                    @foreach($option->child_options as $child_option)
                                                                                        <div class="child-option-block form-check-inline">
                                                                                            @if($option->child_option_type == 'multiple')
                                                                                                <input type="checkbox" name="feedback[{{$section->section_id}}_{{$question->question_id}}_{{$child_option->option_id}}]" id="{{$child_option->option_id}}" value="{{$child_option->option_id}}" class="{{$child_option->disabled_question_ids}}">
                                                                                                <label class="" for="{{$child_option->option_id}}">&nbsp;{{$child_option->option_text}}</label>
                                                                                            @else
                                                                                                <input type="radio" name="feedback[{{$section->section_id}}_{{$question->question_id}}_xx]" id="{{$child_option->option_id}}" value="{{$child_option->option_id}}" class="{{$child_option->disabled_question_ids}}">
                                                                                                <label class="" for="{{$child_option->option_id}}">&nbsp;&nbsp;{{$child_option->option_text}}</label>
                                                                                            @endif
                                                                                        </div>
                                                                                    @endforeach
                                                                                </div>
                                                                            @elseif ($question->question_type == 'single')
                                                                                <input type="radio" name="feedback[{{$section->section_id}}_{{$question->question_id}}_x]" id="{{$option->option_id}}" value="{{$option->option_id}}" class="{{$option->disabled_question_ids}}">
                                                                                <label class="control-label" for="{{$option->option_id}}">&nbsp;{{$option->option_text}}</label>
                                                                                <div class="form-check-inline child-option child-option-border" style="display: none">
                                                                                    @foreach($option->child_options as $child_option)
                                                                                        <div class="child-option-block form-check-inline">
                                                                                            @if($option->child_option_type == 'multiple')
                                                                                                <input type="checkbox" name="feedback[{{$section->section_id}}_{{$question->question_id}}_{{$child_option->option_id}}]" id="{{$child_option->option_id}}" value="{{$child_option->option_id}}" class="{{$child_option->disabled_question_ids}}">
                                                                                                <label class="" for="{{$child_option->option_id}}">&nbsp;&nbsp;{{$child_option->option_text}}</label>
                                                                                            @elseif($option->child_option_type == 'single')
                                                                                                <input type="radio" name="feedback[{{$section->section_id}}_{{$question->question_id}}_xx]" id="{{$child_option->option_id}}" value="{{$child_option->option_id}}" class="{{$child_option->disabled_question_ids}}">
                                                                                                <label class="" for="{{$child_option->option_id}}">&nbsp;&nbsp;{{$child_option->option_text}}</label>
                                                                                                <div class="form-check-inline child-option-border" style="display: none">
                                                                                                    @foreach($child_option->child_options as $child_of_child)
                                                                                                        <input type="checkbox" name="feedback[{{$section->section_id}}_{{$question->question_id}}_{{$child_of_child->option_id}}]" id="{{$child_of_child->option_id}}" value="{{$child_of_child->option_id}}" class="{{$child_of_child->disabled_question_ids}}">
                                                                                                        <label class="" for="{{$child_of_child->option_id}}">&nbsp;&nbsp;{!! $child_of_child->option_text!!}</label>
                                                                                                    @endforeach
                                                                                                </div>
                                                                                            @else
                                                                                                <div class="col-md-12">
                                                                                                    <div class="form-group row">
                                                                                                        <div class="">
                                                                                                            <label class="" for="{{$child_option->option_id}}">{{$child_option->serial}}.&nbsp;{{$child_option->option_text}}</label>
                                                                                                        </div>
                                                                                                        <div class="">
                                                                                                            <input type="number" min="0" max="5"  name="feedback[{{$section->section_id}}_{{$question->question_id}}_{{$child_option->option_id}}_value]" id="{{$child_option->option_id}}" value="" class="{{$child_option->disabled_question_ids}}">
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            @endif
                                                                                        </div>
                                                                                    @endforeach
                                                                                </div>
                                                                            @else
                                                                                <div class="col-md-4">
                                                                                    <div class="form-group row">
                                                                                        <div class="col-md-6">
                                                                                            <label for="{{$option->option_id}}">{{$option->serial}}.&nbsp;{{$option->option_text}}</label>
                                                                                        </div>
                                                                                        <div class="col-md-4 col-sm-2">
                                                                                            <input type="number" min="0" max="5"  class="form-control" name="feedback[{{$section->section_id}}_{{$question->question_id}}_x]" id="{{$option->option_id}}">
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            @endif
                                                                        </div>
                                                                    @endforeach
                                                                    <div class="option-block ">
                                                                        @foreach($question->extra_options as $extra_option)
                                                                            <div class="col-md-4">
                                                                                <div class="form-group row">
                                                                                    <div class="col-md-6">
                                                                                        <label for="{{$extra_option->option_id}}">{{$extra_option->serial}}.&nbsp;{{$extra_option->option_text}}</label>
                                                                                    </div>
                                                                                    <div class="col-md-4 col-sm-2">
                                                                                        <input type="number" min="0" max="5" class="form-control" name="feedback[{{$section->section_id}}_{{$question->question_id}}_{{$extra_option->option_id}}_value]" id="{{$extra_option->option_id}}" value="{{$extra_option->feedback_option_id}}">
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        @endforeach
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                                <div class="form-group row">
                                    <div class="col-sm-3 col-sm-offset-5">
                                        {{--<button id="submit" type="button" class="btn btn-primary">--}}
                                            {{--Submit--}}
                                        {{--</button>--}}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script>
        $(document).ready(function () {
            // close all
            $('.panel-heading span.clickable').parents('.panel').find('.panel-body').slideUp();
            $('.panel-heading .panel-title').addClass('panel-collapsed');
            $('.panel-heading span.clickable').closest('.panel-heading').find('.btn-arrow').removeClass('fa-chevron-up').addClass('fa-chevron-down');
            $('.panel-heading span.clickable').closest('.panel-heading').find('.btn-plus').removeClass('fa-minus').addClass('fa-plus');

            //open first
            $('.panel-heading span.clickable:first').parents('.panel').find('.panel-body').slideDown();
            $('.panel-heading .panel-title:first').removeClass('panel-collapsed');
            $('.panel-heading span.clickable:first').closest('.panel-heading').find('.btn-plus').removeClass('fa-plus').addClass('fa-minus');
            $('.panel-heading span.clickable:first').closest('.panel-heading').find('.btn-arrow').removeClass('fa-chevron-down').addClass('fa-chevron-up');

            // tangle click
            $('.panel-title').click(function (e) {
                var $this = $(this);
                if (!$this.hasClass('panel-collapsed')) {
                    $this.closest('.panel').find('.panel-body').slideUp();
                    $this.addClass('panel-collapsed');
                    $this.closest('.panel-heading').find('.btn-arrow').removeClass('fa-chevron-up').addClass('fa-chevron-down');
                    $this.closest('.panel-heading').find('.btn-plus').removeClass('fa-minus').addClass('fa-plus');
                } else {
                    $this.closest('.panel').find('.panel-body').slideDown();
                    $this.removeClass('panel-collapsed');
                    $this.closest('.panel-heading').find('.btn-plus').removeClass('fa-plus').addClass('fa-minus');
                    $this.closest('.panel-heading').find('.btn-arrow').removeClass('fa-chevron-down').addClass('fa-chevron-up');
                }
            });
            // geo code
            $("#zilla_id,#upazilla_id,#facility_id").select2({
                placeholder: "-- Select --",
                allowClear: true
            });
            $("#union_id").select2({
                placeholder: "Select Union",
                allowClear: true
            });
            //Load Zilla Data
            $.ajax({
                url: "{{ route('/api/get_geo_code')}}",
                cache: false,
                success: function (result) {
                    $('#zilla_id').select2({
                        data: JSON.parse(result),
                        placeholder: "Select District",
                        allowClear: true
                    }).val("{{ old('zilla_id') }}").trigger('change');

                }
            });
            //load Upazila data
            $('#zilla_id').select2().on('change', function() {
                $('#upazilla_id').find('option').remove();
                $('#union_id').find('option').remove();
                $('#facility_id').find('option').remove();
                var zilla = $('#zilla_id').val();
                if(zilla == null){
                    return ;
                }
                $.ajax({
                    url: "{{ route('/api/get_geo_code')}}?ZillaId="+zilla,
                    cache: false,
                    success: function (result) {
                        $('#upazilla_id').select2({
                            data: JSON.parse(result),
                            placeholder: "Select Upazila",
                            allowClear: true
                        }).val("{{ old('upazilla_id') }}").trigger('change');
                    }
                });
            });
            //load Union data
            $('#upazilla_id').select2().on('change', function() {
                $('#union_id').find('option').remove();
                var zilla = $('#zilla_id').val();
                var upa_zilla = $('#upazilla_id').val();
                $('#facility_id').find('option').remove();
                if(zilla == null || upa_zilla == null){
                    return ;
                }
                $.ajax({
                    url: "{{ route('/api/get_geo_code')}}?ZillaId="+zilla+"&UpazilaId="+upa_zilla,
                    cache: false,
                    success: function (result) {
                        $('#union_id').select2({
                            data: JSON.parse(result),
                            placeholder: "Select Union",
                            allowClear: true
                        }).val("{{ old('union_id') }}").trigger('change');
                    }
                });
            });
            //load Facility name
            $('#union_id').select2().on('change', function() {
                $('#facility_id').find('option').remove();
                var zilla = $('#zilla_id').val();
                var upa_zilla = $('#upazilla_id').val();
                var union_id = $('#union_id').val();
                if(zilla == null || upa_zilla == null|| union_id == null){
                    return ;
                }
                $.ajax({
                    url: "{{ route('/api/get_geo_code')}}?ZillaId="+zilla+"&UpazilaId="+upa_zilla+"&UnionId="+union_id,
                    cache: false,
                    success: function (result) {
                        $('#facility_id').select2({
                            data: JSON.parse(result),
                            placeholder: "Select Facility",
                            allowClear: true
                        }).trigger('change');
                    }
                });
            });
            //child option show/hide
            $('.option-block > input, .child-option-block > input').on('ifToggled', function(event){
                var ids = $(this).prop('class');
                if(ids != '' && ids.indexOf('[') != -1 ) {
                    ids = JSON.parse(ids);
                    for(var i=0;i<ids.length;i++){
                        $('.'+ids[i]).closest('div').closest('.survey-question').toggle('show');
                        if(ids[i].indexOf('XX') != -1){
                            $('#'+ids[i].split('XX')[1]).closest('div').closest('.option-block').toggle('show');
                        }else if(ids[i].indexOf('RR') != -1){
                            $('#'+ids[i].split('RR')[0]).val(ids[i].split('RR')[1]).prop('readonly', true);;
                        }else if(ids[i].indexOf('FF') != -1){
                            $('#'+ids[i].split('FF')[0]).val("").prop('readonly', false);
                        }
                        $('.'+ids[i]).find('input').iCheck('uncheck');
                    }
                }
                $(this).on('ifUnchecked',function () {
                    jQuery(this).closest('div').siblings('.form-check-inline').find('input').iCheck('uncheck');
                    jQuery(this).closest('div').siblings('.form-check-inline:has(input)').hide();
                });
                jQuery(this).closest('div').siblings('.form-check-inline:has(input)').show();


//                $(this).on('ifUnchecked',function () {
//                    jQuery(this).closest('div').siblings('.form-check-inline').find('input').iCheck('uncheck');
//                });
//                jQuery(this).closest('div').siblings('.form-check-inline:has(input)').addClass('child-option-border');
//                jQuery(this).closest('div').siblings('.form-check-inline:has(input)').toggle('show');
            });

            //checked previous result
            $('#facility_id').on("change",function () {
                $(':radio').iCheck('uncheck');
                $(':checkbox').iCheck('uncheck');
                var facility_id = $(this).val();
                if (facility_id != null) {
                    $.ajax({
                        url: "{{ route('/api/get_latest_submission')}}?facility_id=" + facility_id,
                        cache: false,
                        async:false,
                        success: function (result) {
                            var zilla = $('#zilla_id option:selected').text();
                            var upazila = $('#upazilla_id option:selected').text();
                            var union = $('#union_id option:selected').text();
                            var facility = $('#facility_id option:selected').text();
                            $('.district-name').html(zilla);
                            $('.upazila-name').html(upazila);
                            $('.union-name').html(union);
                            $('.facility-name').html(facility);
                            $('#facility-info').show();
                            var latest_submission = result.replace(/&quot;/g, '"');
                            latest_submission = JSON.parse(latest_submission);
                            latest_submission = latest_submission['data'];
                            var score = 0;
                            for (var i = 0; i < latest_submission.length; i++) {
                                if (latest_submission[i]['feedback_option_id'] != '77777') {
                                    score += latest_submission[i]['value'];
                                    $('#' + latest_submission[i]['feedback_option_id']).iCheck('check');
                                    $('#' + latest_submission[i]['feedback_option_id']).val(latest_submission[i]['feedback_text']);
//                                    $('#' + latest_submission[i]['feedback_option_id']).closest('.option-block').find('.form-check-inline:has(input)').addClass('child-option-border');
                                    $('#' + latest_submission[i]['feedback_option_id']).closest('.form-check-inline').show();
                                }
                            }
                            $(':radio').attr('disabled', true);
                            $(':checkbox').attr('disabled', true);
                            $('input[type="number"]').prop('readonly', true);
                            var cat = '';
                            $('.facility-score').html(score);
                            if(score>200){
                                cat='A';
                            }else if(score<201 && score>100){
                                cat='B';
                            }else if(score<101){
                                cat='C';
                            }
                            $('.facility-category').html(cat);
                        }
                    });
                }
            });

            // back button
            $('.form-history-back').on('click', function (event) {
                event.preventDefault();
                history.back(1);
            });


        });
    </script>
@endsection