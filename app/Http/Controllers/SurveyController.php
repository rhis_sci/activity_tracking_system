<?php

namespace App\Http\Controllers;

use App\Facility;
use App\Options;
use App\Question;
use App\QuestionsOptionsMapping;
use App\Section;
use App\Zilla;
use App\User;
use App\Union;
use App\Upazila;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Mockery\Exception;

class SurveyController extends Controller
{

    public function index(Request $request) {
        try {
            $str = (Input::get('url64', ''));
            if ($str != '') {
                $str = base64_decode($str);
                $json_str = json_decode($str, true);
                $view = "";
                $device = $json_str['device'];
                $facility_id = $json_str['facility_id'];
                $user_id = $json_str['user_id'];
                $token = $json_str['token'];
                $user = User::find($user_id);
                if($user != null){
                    Auth::login($user, true);
                }
                if ($device !='ANDROID' || $token != Auth::user()->token || $facility_id != Auth::user()->facility_id){
                    return redirect('/go404');
                }
                $view = "survey_for_android";
            } else {
                $facility_id = Input::get('facility_id', 0);
                $view = "survey";
                $geo = array();
            }
            $sections = Section::with('questions', 'questions.options', 'questions.options.child_options', 'questions.options.child_options.child_options')->orderBy('serial', 'asc')->get();
            return view($view, ['sections' => $sections, 'geo' => '', 'data' => $this->getLatestData($facility_id)]);
        }catch (Exception $e){
            return redirect('/go404');
        }
    }
    public function getLatestData($facility_id=0){
        $facility_id = Input::has('facility_id') ? Input::get('facility_id', '###') : $facility_id;
        $latest_submission = array();
        $last_time = QuestionsOptionsMapping::select('created_at')->where('facility_id','=',$facility_id)->orderBy('created_at','desc')->first();
        if(!is_null($last_time)){
            $latest_submission = QuestionsOptionsMapping::select('feedback_option_id','value','feedback_text')->where('facility_id','=',$facility_id)->where('created_at','=',$last_time->created_at)->get();
        }
        return json_encode(array('date_of_submission'=>$last_time,'data'=>$latest_submission));
    }

//    public function getPreviousData(){
//        $facility_id = Input::has('facility_id') ? Input::get('facility_id', '###') : '###';
//        $previous_submission = array();
//        $previous_time = QuestionsOptionsMapping::where('facility_id','=',$facility_id)->orderBy('created_at', 'desc')->skip(1)->take(1)->first();
//        if(!is_null($previous_time)){
//            $previous_submission = QuestionsOptionsMapping::select('feedback_option_id','value','feedback_text')->where('facility_id','=',$facility_id)->where('created_at','=',$previous_time->created_at)->get();
//        }
//        return json_encode(array('date_of_submission'=>$previous_time,'data'=>$previous_submission));
//    }
    public function calculatePoint($id, $all){

        foreach($all as $single)
            if($single->option_id == $id)
                return $single->option_value;
    }

    public function store(Request $request){
        if ($request->ajax()) {
            $facility_id = $request->data['facility_id'];
            if(!ctype_digit($facility_id)){
                return response()->json([
                    'outcome' => 'failed',
                    'msg' => 'Invalid Facility',
                    'errorMsg' => "Invalid Facility"
                ]);
            }
            DB::beginTransaction();
            try {
                $feedbacks = $request->data;
                $obj = new QuestionsOptionsMapping;
                $all_options = Options::all();
                $dataSet = [];
                $feedback_question_ids = [];
                $created_time = date("Y-m-d H:i:s");
                if(array_key_exists('feedback', $feedbacks)){
                    foreach ($feedbacks['feedback'] as $key => $feedback){
                        $section_id = explode("_",$key)[0];
                        $question_id = explode("_",$key)[1];
                        array_push($feedback_question_ids,$question_id);
                        $feedback_text = "";
                        if (strpos($key, 'value') !== false) {
                            $feedback_option_id = (int)explode("_",$key)[2];
                            $feedback_text = $feedbacks['feedback'][$key];
                        }else{
                            $feedback_option_id = (int)$feedbacks['feedback'][$key];
                        }

                        array_push($dataSet,[
                            'facility_id'  => $facility_id,
                            'feedback_of_question_id' => (int)$question_id,
                            'feedback_option_id' => $feedback_option_id,
                            'section_id' => (int)$section_id,
                            'feedback_text' => $feedback_text,
                            'value'       => $this->calculatePoint((int)$feedbacks['feedback'][$key],$all_options),
                            'created_at'       => $created_time,
                            'created_by' => Auth::user()->user_id
                        ]);
                    }
                }
                $not_feedbacked_ids = Question::select('question_id','section_id')->whereNotIn('question_id',$feedback_question_ids)->get();
                foreach ($not_feedbacked_ids as $id){
                    array_push($dataSet,[
                        'facility_id'  => $facility_id,
                        'feedback_of_question_id' => $id->question_id,
                        'feedback_option_id' => 77777,
                        'feedback_text' => "",
                        'section_id' => $id->section_id,
                        'value'       => null,
                        'created_at'       => $created_time,
                        'created_by' => Auth::user()->user_id
                    ]);
                }
                $obj->insert($dataSet);
            } catch (Exception $e){
                DB::rollback();
                return response()->json([
                    'outcome' => 'failed',
                    'msg' => 'Exception Raised',
                    'errorMsg' => 'Save Not Successfully',
                ]);
            }

            DB::commit();
            return response()->json([
                'outcome' => 'success',
                'msg' => 'Saved successfully',
                'id' => "ss",
                'mode' => "mod",
                'data' =>json_decode($this->getLatestData($facility_id)),
            ]);
        }
    }


}
