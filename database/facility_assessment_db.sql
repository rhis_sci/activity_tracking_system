create table "Division"
(
  id text not null
    constraint "Division_pkey"
    primary key,
  division text
)
;

create table "Mouza"
(
  "ZillaId" text,
  "UpazilaId" text,
  "MunicipalityId" text,
  "UnionId" text,
  "MouzaId" text,
  "RMO" text,
  "MouzaNameEng" text,
  "MouzaName" text
)
;

create table "Unions"
(
  "ZillaId" text not null,
  "UpazilaId" text not null,
  "MunicipalityId" text,
  "UnionId" text not null,
  "UnionNameEng" text,
  "UnionName" text,
  constraint "Unions_pkey"
  primary key ("ZillaId", "UpazilaId", "UnionId")
)
;

create table "Upazila"
(
  "ZillaId" text not null,
  "UpazilaId" text not null,
  "UpazilaNameEng" text,
  "UpazilaName" text,
  constraint "Upazila_pkey"
  primary key ("ZillaId", "UpazilaId")
)
;

create table "Village"
(
  "ZillaId" text not null,
  "UpazilaId" text not null,
  "UnionId" text not null,
  "MouzaId" text not null,
  "VillageId" text not null,
  "RMO" text,
  "VillageNameEng" text,
  "VillageName" text,
  "CRRVillageName" text,
  close text,
  constraint "Village_pkey"
  primary key ("ZillaId", "UpazilaId", "UnionId", "MouzaId", "VillageId")
)
;

create table "Zilla"
(
  "DivId" text not null,
  "ZillaId" text not null,
  "ZillaNameEng" text,
  "ZillaName" text,
  constraint "Zilla_pkey"
  primary key ("DivId", "ZillaId")
)
;

create table users
(
  user_id char(20) not null
    constraint users_pkey
    primary key,
  password varchar(500),
  name varchar(100),
  designation varchar(100),
  email varchar(100),
  phone varchar(11),
  user_type varchar(20),
  is_active smallint,
  address varchar(500),
  zilla_id varchar(2),
  upazilla_id varchar(2),
  union_id varchar(2),
  created_at timestamp,
  updated_at timestamp,
  remember_token varchar(100),
  facility_id integer
)
;

create table section
(
  section_id serial not null
    constraint section_pkey
    primary key,
  section_name varchar(100),
  details varchar(500),
  created_at timestamp default now() not null,
  updated_at timestamp default now() not null,
  created_by integer,
  updated_by integer,
  serial integer,
  section_code varchar(10)
)
;

create table questions
(
  question_id serial not null
    constraint questions_pkey
    primary key,
  question_text varchar(500),
  question_serial integer,
  question_type varchar(20),
  question_code varchar(20),
  is_inactive smallint,
  created_at timestamp default now() not null,
  updated_at timestamp default now() not null,
  created_by integer,
  updated_by integer,
  section_id integer
)
;

create table options
(
  option_id serial not null
    constraint options_pkey
    primary key,
  option_text varchar(500),
  option_value integer,
  question_id integer,
  parent_id integer,
  child_option_type varchar(10),
  serial integer
)
;

create table questions_options_mapping
(
  questions_options_mapping_id serial not null
    constraint questions_options_mapping_questions_options_mapping_id_pk
    primary key,
  facility_id integer,
  feedback_text varchar(250),
  value integer,
  feedback_of_question_id integer,
  feedback_option_id integer,
  created_by integer,
  created_at timestamp,
  section_id integer
)
;

create table old_data
(
)
;

create table facility_registry
(
  zillaid varchar(2) not null,
  upazilaid varchar(3) not null,
  unionid varchar(3) not null,
  mouzaid integer,
  villageid integer,
  facilityid serial not null,
  facility_type text,
  lat text,
  lon text,
  facility_name text,
  facility_category text,
  facility_type_id integer,
  id char,
  facility_owner varchar(20),
  constraint facility_registry__pk3
  primary key (zillaid, upazilaid, unionid, facilityid)
)
;

create table questions_options_mapping_final
(
  questions_options_mapping_id serial not null
    constraint questions_options_mapping_questions_options_mapping_id_pk2
    primary key,
  facility_id integer,
  feedback_text varchar(250),
  value integer,
  feedback_of_question_id integer,
  feedback_option_id integer,
  created_by varchar(20),
  created_at timestamp,
  section_id integer
)
;

