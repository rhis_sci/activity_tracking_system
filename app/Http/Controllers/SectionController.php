<?php

namespace App\Http\Controllers;

use App\Facility;
use App\Section;
use App\Zilla;
use App\Union;
use App\Upazila;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Mockery\Exception;

class SectionController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax() && $request->has('rows')) {
            $per_page = $request->input('rows','10');
            $sort = Input::has('sidx') ? Input::get('sidx', 'section_id') : 'section_id';
            $sord = $request->input('sord','asc');
            $filters = Input::has('filters') ? Input::get('filters', []) : [];
            $op = array("eq" =>"=", "ne"=>"!", "lt"=>"<", "le"=>"<=", "gt"=>">", "ge"=>">=","cn"=>"LIKE");
            $query = Section::select("section_id","section_name","details")->orderBy($sort,$sord);
            if(Input::has('filters')){
                $filters = json_decode($filters);
                foreach ($filters->rules as $filter){
                    if($filter->op == 'cn'){
                        $query->where($filter->field,$op[$filter->op],"%".$filter->data."%");
                    }else{
                        $query->where($filter->field,$op[$filter->op],$filter->data);
                    }
                }
            }
            $users = $query->paginate($per_page);
            $data = array(
                'page' => $users->currentPage(),
                'rows' => $users->items(),
                'records' => $users->total(),
                'total' => $users->lastPage()
            );
            return response()->json($data);
        }
        return view('section_list');
    }
    public function create(Request $data)
    {

        return view('section_add');
    }
 public function store(Request $data){
//     dd($data);
     if($data->method() == 'POST'){
         Section::create([
//            'section_id' => $data->section_id,
             'section_name' => $data->section_name,
             'details' => $data->details,
         ]);
     }
     return redirect('section');
 }
}
