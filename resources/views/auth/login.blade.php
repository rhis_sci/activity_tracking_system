<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Facility Assessment | Login</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
{{--    <link href="{{ asset('public/css/bootstrap.min.css') }}" rel="stylesheet">--}}
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ asset("font-awesome/css/font-awesome.min.css") }}">
    <link href="{{ asset('public/css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('public/css/common.css') }}" rel="stylesheet">
</head>
<body class="hold-transition skin-blue sidebar-mini"
      style="background: #d2d6de;">
<div id="wrapper">
    <div class="login-box" id="remove_div">
        <div class="center-block">
            <br>
            <br>
            <br>
            <br>
        </div>
        <style>
        </style>
        <div class="login-box-body">
            <div class="row">
                <div class="center-block">
                    <h1 style="text-align: center;margin-top: -2px;margin-bottom: 15px;font-size: 38px;text-shadow: 4px 4px 3px #e2e2e2;">
                        <b>
                            <span style="color:#ffc716">Activity</span>
                            <span style="color:#2276b7">Tracking</span>
                        </b>
                    </h1>
                </div>
            </div>
            <div id="error_msg" style="color: red"></div>
            <form id="loginData " class="form-signin"  method="POST" action="{{ route('login') }}">
                {{ csrf_field() }}
                <div class="form-group {{ $errors->has('user_id') ? ' has-error' : '' }}">
                    @if ($errors->has('user_id'))
                        <span class="help-block">
                                   <strong>{{ $errors->first('user_id') }}</strong>
                             </span>
                    @endif
                    <div class="input-group">
							<span class="input-group-addon">
	                            <i class="fa fa-user" aria-hidden="true"></i>
	                        </span>
                        <input type="text" id="user_id" name="user_id" class="form-control" value="{{ old('user_id') }}"
                               placeholder="User ID" />
                    </div>
                </div>
                <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                    @if ($errors->has('password'))
                        <span class="help-block">
                                 <strong>{{ $errors->first('password') }}</strong>
                             </span>
                    @endif
                    <div class="input-group">
							<span class="input-group-addon">
	                            <i class="fa fa-lock" aria-hidden="true"></i>
	                        </span>
                        <input type="password" id="password" name="password" class="form-control"
                               placeholder="Password" />

                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <input type="submit" id="login"
                               class='btn btn-primary btn-block btn-flat' value="Log in" />
                    </div>
                </div>
            </form>
            <a href='#'>Forgot password?</a>
        </div>
    </div>
</div>
</body>
{{--<script src="{{ asset("js/popper.min.js")}}" type="application/javascript"></script>--}}
<!-- jQuery  -->
{{--<script src="{{ asset("js/jquery.min.js")}}" type="application/javascript"></script>--}}
<!-- Bootstrap 3.3.5 -->
{{--<script src="{{ asset('public/js/bootstrap.min.js') }}" type="application/javascript"> </script>--}}
<script src="{{ asset("public/js/common.js")}}" type="application/javascript"></script>
</body>
</html>
