@extends('index')
@section('content')
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">Section Add</h3>
                        <div class="box-tools">
                            <div class="btn-group pull-right" style="margin-right: 10px">
                                <a href="{{ url('section') }}" class="btn btn-sm btn-default"><i class="fa fa-list"></i>Section List</a>
                            </div>
                            <div class="btn-group pull-right" style="margin-right: 10px">
                                <a class="btn btn-sm btn-default form-history-back"><i class="fa fa-arrow-left"></i>&nbsp;Back</a>
                            </div>
                        </div>
                    </div>
                    <form class="form-horizontal" method="POST" action="{{ url('section') }}">
                        {{ csrf_field() }}
                        <div class="box-body">
                            <div class="fields-group">
                                <div class="form-group row {{ $errors->has('section_name') ? ' has-error' : '' }}">
                                    <label for="section_name" class="control-label col-sm-2 inline-label">Section Name</label>
                                    <div class="col-sm-8">
                                        @if ($errors->has('section_name'))
                                            <label class="control-label" for="inputError"><i
                                                        class="fa fa-times-circle-o"></i> {{ $errors->first('section_name') }}
                                            </label><br/>
                                        @endif
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                            <input type="text" id="section_name" name="section_name" value="{{ old('section_name') }}"
                                                   class="form-control" placeholder="Section Name" required autofocus>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row {{ $errors->has('details') ? ' has-error' : '' }}">
                                    <label for="details" class="control-label col-sm-2 inline-label">Details</label>
                                    <div class="col-sm-8">
                                        @if ($errors->has('details'))
                                            <label class="control-label" for="inputError"><i
                                                        class="fa fa-times-circle-o"></i> {{ $errors->first('details') }}
                                            </label><br/>
                                        @endif
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                            <input type="text" id="details" name="details" value="{{ old('details') }}"
                                                   class="form-control" placeholder="Details" required autofocus>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-md-2"></div>
                                    <div class="col-md-3 col-md-offset-5">
                                        <button type="submit" class="btn btn-primary">
                                            Submit
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script>
        $(function () {
            $('.form-history-back').on('click', function (event) {
                event.preventDefault();
                history.back(1);
            });

        });
    </script>
@endsection