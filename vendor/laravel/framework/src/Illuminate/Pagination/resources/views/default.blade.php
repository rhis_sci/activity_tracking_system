
@if ($paginator->hasPages())
    <ul class="pagination pagination-sm no-margin pull-right">
        {{--<label class="control-label pull-right" style="margin-right: 10px; font-weight: 100;">--}}
            {{--<small>Show</small>&nbsp;--}}
            {{--<select class="input-sm grid-per-pager" name="per-page">--}}
                {{--<option value="http://localhost:8000/register/list?per_page=1">5</option>--}}
                {{--<option value="http://localhost:8000/register/list?per_page=2">10</option>--}}
                {{--<option value="http://localhost:8000/register/list?per_page=3">15</option>--}}
            {{--</select>--}}
            {{--&nbsp;<small>entries</small>--}}
        {{--</label>--}}
        {{-- Previous Page Link --}}
        @if ($paginator->onFirstPage())
            <li class="page-item disabled"><span class="page-link">&laquo;</span></li>
        @else
            <li class="page-item"><a class="page-link" href="{{ $paginator->previousPageUrl() }}" rel="prev">&laquo;</a>
            </li>
        @endif

        @foreach ($elements as $element)
        <!-- "Three Dots" Separator -->
            @if (is_string($element))
                <li class="page-item disabled"><span class="page-link">{{ $element }}</span></li>
            @endif

        <!-- Array Of Links -->
            @if (is_array($element))
                @foreach ($element as $page => $url)
                    @if ($page == $paginator->currentPage())
                        <li class="page-item active"><span class="page-link">{{ $page }}</span></li>
                    @else
                        <li class="page-item"><a class="page-link" href="{{ $url }}">{{ $page }}</a></li>
                    @endif
                @endforeach
            @endif
        @endforeach

        {{-- Next Page Link --}}
        @if ($paginator->hasMorePages())
        <li class="page-item"><a class="page-link" href="{{ $paginator->nextPageUrl() }}" rel="next">&raquo;</a></li>
        @else
        <li class="page-item disabled"><span class="page-link">&raquo;</span></li>
        @endif
    </ul>
@endif
