@extends('index')

@section('content')
    <section class="content-header">


    </section>

    <section class="content">
        <!-- issue Details Modal -->
        <div  class="row">



            <!-- mid column -->
            <div id="Issue_detatils" class="col-md-9">

                <!-- facility detail -->
                <div class="box box-success">

                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-6">
                                <h4><i class="fa fa-bed"></i> <span id="facility-name"></span></h4>
                            </div>
                            <div class="col-md-6">
                                <!--<i class="fa fa-cubes"></i> <span id="facility-category"></span>-->
                            </div>
                        </div>
                    </div>
                </div>
                <!-- //facility detail -->

                <div class="row" id="comment-panel">
                    <!-- Comment comumn -->
                    <div class="col-md-12">
                        <!-- search result -->
                        <div class="box box-info">
                            <div class="box-header with-border">
                                <h3 class="box-title">Comments/Tasks</h3>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body" id="comment-body">
                                <!-- comment -->

                                @foreach($comments as $row)


                                    <div class="direct-chat-msg" >
                                        <div class="direct-chat-info clearfix">
                                            <span class="direct-chat-name pull-left">{{$row->name}}</span>
                                            <span class="direct-chat-timestamp pull-right">{{$row->created_at}}</span>
                                        </div>

                                        @if ($row->status == 'resolved')
                                            <img class="direct-chat-img" title="Issue Resolved" src="{{asset('public/image/issue-completed.png')}}">

                                        @elseif ($row->status == 'postpone')
                                            <img class="direct-chat-img" title="Issue Postpone" src="{{asset('public/image/issue-postpone.png')}}">

                                        @else ($row->status == '')
                                            <img class="direct-chat-img" title="Issue" src="{{asset('public/image/issue.png')}}">
                                            {{--<button class="btn btn-success btn-xs issue-status" data-issueid="{{$row->id}}" data-issuestatus="{{$row->status}}" type="button">
                                                <span class="fa fa-flag-checkered"></span></button>--}}
                                        @endif

                                        <div class="direct-chat-text">{{$row->details}}  {{--+ '' +	{{$row->reply}}--}}
                                            @if ($row->resolved != "" && $row->resolved != null)
                                                <div style="background-color: #FFF"><span class="fa fa-reply"></span>{{$row->resolved}}</div>
                                            @endif
                                        </div>

                                        <div class="direct-chat-btn">

                                            <!--<button class="btn btn-xs reply-issue" data-parentid={{$row->id}}><span class="fa fa-reply"></span></button> -->
                                            <!--<button class="btn btn-success btn-xs issue-status" data-issueid="{{$row->id}}" data-issuestatus="{{$row->status}}" type="button">-->

                                                <!--<span class="fa fa-flag-checkered"></span></button> -->

                                            @foreach($row->child as $child)
                                                {{--{{var_dump ($child)}}--}}
                                                <hr><div style="padding: 5px; margin-bottom: 3px; background-color: #dddddd; border-bottom: solid 1px #ccc;">{{$child->details}}
                                                    <br><small><i class="fa fa-user-circle"></i> {{$child->name}} &nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-calendar"></i> {{$child->created_at}}</small>
                                                </div>

                                            @endforeach
                                        </div>
                                    </div>

                                    @endforeach
                                    </table>
                                    <!--/comments -->
                            </div>
                            <!-- /.box-body -->

                        {{--<div class="box-footer">
                            <div class="col-md-12">
                                <div class="input-group">

                                  <span class="input-group-btn">
                                      <button class="btn btn-danger" type="button" id="comments-to-add-btn"><span class="fa fa-plus"></span> Comment / Issue</button>
                                  </span>
                                </div><!-- /input-group -->
                            </div>
                        </div>--}}
                        <!-- /.box-footer-->
                        </div>
                    </div>
                </div>
            </div>
            <!-- /mid comumn -->
        </div>
        <!-- issue Details Modal -->


        <!-- issue Modal -->
        <!-- Modal -->
        <div class="modal fade" id="modal-issue" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header bg-aqua">
                        <div class="modal-title pull-left" id="">Create New Issue</div>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

                    </div>
                    <div class="modal-body">
                        <!-- form -->
                        <form>
                            <!--<div class="form-group">
                                <label for="" class="text-bold">Title</label>
                                <input type="text" class="form-control" id="issue-title" placeholder="">
                            </div>-->

                            <div class="form-group">
                                <label for="" class="text-bold">Category</label>
                                <select class="form-control" id="issue-category">
                                    <option selected="selected" value="comment">Comment</option>
                                    <option value="issue">Issue</option>

                                </select>
                            </div>

                            <div id="issue-period" style="display:none;">
                                <div class="form-group">
                                    <label for="" class="text-bold">Issue Create Date (M-D-Y)</label>
                                    <input type="text" class="form-control" id="issue-create-date" placeholder="">
                                </div>

                                <div class="form-group">
                                    <label for="" class="text-bold">Issue Completion Date (M-D-Y)</label>
                                    <input type="email" class="form-control" id="issue-completion-date" placeholder="">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="" class="text-bold" style="display: block">Tag Others</label>
                                <textarea class="form-control" id="tags"></textarea>
                            </div>

                            <div class="form-group">
                                <label for="" class="text-bold">Priority</label>
                                <select class="form-control" id="issue-priority">
                                    <option selected="selected">Normal</option>
                                    <option>Low</option>
                                    <option>High</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for=""  class="text-bold">Description</label>
                                <textarea class="form-control" id="issue-detail"></textarea>
                            </div>



                        </form>
                        <!-- /form -->
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-danger btn-sm" id="create-issue">Create</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- /issue Modal -->


        <!-- issue status change Modal -->
        <!-- Modal -->
        <div class="modal fade" id="modal-issue-status" tabindex="-1" role="dialog" aria-labelledby="">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header bg-aqua">
                        <div class="modal-title pull-left" id="">Issue</div>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

                    </div>
                    <div class="modal-body">
                        <!-- form -->
                        <form>

                            <div class="form-group">
                                <label for="" class="text-bold">Status</label>
                                <select class="form-control" id="issue-status">
                                    <option value="ongoing">Pending</option>
                                    <option value="postpone">Postpone</option>
                                    <option value="resolved">Resolved</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="issue-update-detail" class="text-bold">Detail</label>
                                <textarea id="issue-update-detail" class="form-control"></textarea>

                            </div>



                        </form>
                        <!-- /form -->
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary btn-sm" id="change-status">Save</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- /issue status change Modal -->

        <!-- Reply -->
        <div class="modal fade" id="modal-reply" tabindex="-1" role="dialog" aria-labelledby="">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header bg-aqua">
                        <div class="modal-title pull-left" id=""><i class="fa fa-reply"></i> Reply</div>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

                    </div>
                    <div class="modal-body">
                        <!-- form -->
                        <form>
                            <input type="hidden" id="reply_parent_id">
                            <div class="form-group">
                                <textarea id="issue-reply-txt" class="form-control"></textarea>
                            </div>



                        </form>
                        <!-- /form -->
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary btn-sm" id="reply-btn">Save</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- /reply -->


    </section>

    <style>
        .facility_tr{
            cursor:pointer;
        }

    </style>
@endsection
@section('script')
    <script>


    </script>
@endsection