@extends('index')

@section('content')

    <section class="content">
		<h3>Issue list</h3>
	
		<div class="row">
			<!--region-->
			<div class="col-md-12">
				<div class="box">
					<table class="table table-hover">
						<thead>
						<tr>
							<th>Location</th>
							<th>Issue</th>
							<th>Comment</th>
						</tr>
						</thead>
						<tr>
							<td>Dhaka</td>
							<td>100</td>
							<td>10</td>
						</tr>
						<tr>
							<td>Dhaka</td>
							<td>100</td>
							<td>10</td>
						</tr>
						<tr>
							<td>Dhaka</td>
							<td>100</td>
							<td>10</td>
						</tr>

					</table>
				</div>
			</div>
			<!--//region-->

		</div>
    </section>

@endsection
@section('script')
    <script>


    </script>
@endsection