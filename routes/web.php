<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

Auth::routes();


Route::get('/dashboard', 'DashboardController@index');
Route::get('/dashboard/issue', 'DashboardController@issue');
Route::get('/comments', 'commentsController@index');
Route::get('/issue', 'commentsController@issues');
Route::get('/issueDetails/id={id}', 'commentsController@issue_details');

Route::get('/getzilla', 'commentsController@get_zilla');
Route::get('/getupazila', 'commentsController@get_upazila');
Route::get('/getfacility', 'commentsController@get_facility');
Route::get('/get_comments', 'commentsController@get_comments');
Route::get('/add_comments', 'commentsController@add_comments');
Route::get('/update_status', 'commentsController@update_status');
Route::get('/facility_detail', 'detailController@index');
Route::get('/sendmail', 'commentsController@sendMail');

//Mail sent Cron
Route::get('/mailcron', 'MailManager@send_pending_mail');


//Profile
Route::get('/account', 'accountController@index');
Route::get('/account/password_change', 'accountController@reset_pass');



Route::get('/', 'HomeController@index')->name('home');
Route::get('/home', 'HomeController@index')->name('home_1');
Route::get('/api/get_geo_code', 'HomeController@get_geo_code')->name('/api/get_geo_code');
Route::get('/register/list', 'Auth\RegisterController@register_list')->name('register/list');

Route::resource('/section', 'SectionController');
Route::resource('/question', 'QuestionController');
Route::get('/survey', 'SurveyController@index');
Route::post('/survey/submit', 'SurveyController@store');
Route::get('/api/get_latest_submission', 'SurveyController@getLatestData')->name('/api/get_latest_submission');
Route::get('/api/get_previous_submission', 'SurveyController@getPreviousData')->name('/api/get_previous_submission');

Route::get('/go404', 'HomeController@go404')->name('/go404');

//Route::get('/reports/category_details', 'ReportsController@index');
Route::get('/reports/category_details', 'ReportsController@category_compare');
Route::get('/reports/year_wise_category', 'ReportsController@year_wise_category');

Route::get('/live', 'LiveController@index');



