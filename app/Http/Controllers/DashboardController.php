<?php

namespace App\Http\Controllers;

use App\Facility;
use App\Issues;
use App\Zilla;
use App\Union;
use App\Upazila;
use App\Section;
use App\Comment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Mockery\Exception;
use DB;

class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index() {
		
		//return Zilla::all();

        try {


            //$get = DB::statement("select count(*) from issues");
            $get_issue = DB::select ("select count(*) from issues where category = 'issue'");
            $get_issue_ongoing = DB::select ("select count(*) from issues where category = 'issue' and status = 'ongoing'");
            $get_issue_solved = DB::select ("select count(*) from issues where category = 'issue' and status = 'resolved'");


            $get_comment = DB::select ("select count(*) from issues where category = 'comment'");

            $last_login_info = DB::select("select ll.user_id,max(ll.created_at) as lastlogin,u.name as user_name,z.\"ZillaNameEng\" as zilla,
                                count(ll.user_id) as total_login
                                from last_login ll
                                left join users u on u.user_id  = ll.user_id
                                left join \"Zilla\" z on z.\"ZillaId\" = u.zilla_id 
                                group by ll.user_id,user_name, zilla Order by lastlogin desc");

            $data = array(
                'issue' => $get_issue[0]->count,
                'issue_ongoing' => $get_issue_ongoing[0]->count,
                'issue_resolved' => $get_issue_solved[0]->count,
                'comment' => $get_comment[0]->count,
                'last_login_info' => $last_login_info
            );






            //var_dump($data);

            return view('dashboard/dashboard_view',['data'=>$data]);

        }catch (Exception $e){

        }


    }

    public function issue(){
        return view('dashboard/dashboard_issue');
    }
	
	
	
    
}
