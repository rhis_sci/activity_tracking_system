<?php

namespace App\Http\Controllers;


use App\EmailQueue;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Facility;
use App\Zilla;
use App\Union;
use App\Upazila;
use App\Section;
use App\Comment;
use App\Issues;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Mockery\Exception;
use Mail;
use DB;
use mysql_xdevapi\Statement;
use Auth;


class commentsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index() {

		//return Zilla::all();
        return view('commentsView');
    }
	
	public function get_zilla() {
		
		return Zilla::select('*')->where('DivId','=',$_GET['zillaid'])->get();

        //return view('commentsView');
    }
	
	public function get_upazila() {
		
		$zillaid = $_GET['zillaid']<10?"0".$_GET['zillaid']:$_GET['zillaid'];
		
		return Upazila::select('*')->where('ZillaId','=',$zillaid)->get();

        //return view('commentsView');
    }
	
	public function get_facility() {
		
		$zillaid = $_GET['zillaid']<10?"0".$_GET['zillaid']:$_GET['zillaid'];
		$upazilaid = $_GET['upazilaid']<10?"0".$_GET['upazilaid']:$_GET['upazilaid'];

		$get = Facility::select('*')->where('zillaid','=',$zillaid)->where('upazilaid','=',$upazilaid);

		return $get->get();

        //return view('commentsView');
    }
	
	
	public function get_comments() {
		
		$facility_id = $_GET['facility_id'];
		
		//return Comment::select('*')->where('facility_id','=',$facility_id)->get();

        //working
        //return Issues::select('*')->where('facility_id','=',$facility_id)->get();
        //working end

        $comments= DB::select("select * from issues s 
            join users u on u.user_id = s.user_id
            where s.facility_id = $facility_id and child_of = 0 order by s.created_at desc");

        foreach($comments as $key=>$comment){

           $child= DB::select("select * from issues s 
            join users u on u.user_id = s.user_id
            where s.facility_id = $facility_id and child_of = $comment->id");

           $comments[$key]->child = $child;
           
        }
        //var_dump ($comments);
        return $comments;



    }
	
	public function add_comments() {
		
		try{
			$comment = new Issues;
			$comment->facility_id = $_GET['facility_id'];
			$comment->user_id = Auth::id(); //Loggedin USer
			$comment->title = $_GET['title'];
			$comment->details = $_GET['detail'];
			$comment->category = $_GET['category'];
			$comment->priority = $_GET['priority'];
			$comment->tags = $_GET['tags'];
			$comment->create_date = isset($_GET['create_date'])?$_GET['create_date']:date('Y-d-m');
			$comment->completion_date = isset($_GET['completion_date'])?$_GET['completion_date']: date('Y-m-d');
            $comment->child_of = isset($_GET['child_of'])?$_GET['child_of']:0;
            $comment->status = 'ongoing';
			$comment->save();

			$last_inserted_issue_id = $comment->id;

			//add data to mail queue
            //After creating issu/comment following information will store in email table. Another script will run and in cron and send the email
            //From background process. While sending the email, background process will update stats of email table after sending email

            $email = new EmailQueue;
            $email->creator_id = Auth::id();
            $email->issue_id = $last_inserted_issue_id;
            $email->mail_to = Auth::user()->email;
            $email->cc = '';
            $email->bcc ='';
            $email->mail_subject = '';
            $email->mail_body = $comment->details;
            $email->priority =$comment->priority;
            $email->tags =$comment->tags;
            $email->category =$comment->category;
            $email->status = 'pending';

            $email->save();

		}
		catch(Exception $e){
			//return redirect('insert')->with('failed',"operation failed");
		}

    }

    public function update_status()
    {

        try {
            Issues::where('id', $_GET['comment_id'])->where('facility_id', $_GET['facility_id'])
                ->update([
                    'status' => $_GET['status'],
                    'resolved' => $_GET['resolved']
                ]);

            //$this->sendMail($_GET['comment_id']);
        }catch (Exception $e){

        }
    }

    //This function will return a issue/comment with its child (Reply)
    private function getIssueByID($issue_id){

        $issues= DB::select("select * from issues s 
            join users u on u.user_id = s.user_id
            where s.id = $issue_id and child_of = 0 order by s.created_at desc");

        foreach($issues as $key=>$comment){

            $child= DB::select("select * from issues s 
            join users u on u.user_id = s.user_id
            where child_of = $comment->id");

            $issues[$key]->child = $child;

        }
        return $issues;
    }


    public function issues(){

        //If id has a value, it means it will show the detail of an individual issue/comment with all reply
        if(isset($_GET['id'])){
            $issues=$this->getIssueByID($_GET['id']);
            $data = array(
                'issues' => $issues
            );

            return view('issue/issue_single_view',['data'=>$data]);
        }

        //This will show all issues/comments in list
        $issues= DB::select("select 
                                i.id,
                                i.facility_id,
                                i.user_id,
                                i.details,
                                i.category,
                                i.priority,
                                i.tags,
                                i.create_date,
                                i.completion_date,
                                i.status,
                                --case when i.status = 'ongoing' then 'Pending' end \"i.status\",
                                u.name,
                                fr.zillaid,
                                fr.upazilaid,
                                fr.unionid,
                                fr.facility_name,
                                z.\"ZillaNameEng\" as zilla_name,
                                uz.\"UpazilaNameEng\" as upazil_name,
                                un.\"UnionNameEng\" as union_name                         
                                
                                from issues i 
                                left join users u on u.user_id = i.user_id
                                left join facility_registry fr on fr.facilityid = i.facility_id
                                left join \"Zilla\" z on z.\"ZillaId\" = fr.zillaid
                                left join \"Upazila\" uz on uz.\"UpazilaId\" = fr.upazilaid and uz.\"ZillaId\" = fr.zillaid
                                left join \"Unions\" un on un.\"ZillaId\" = fr.zillaid and un.\"UpazilaId\" = fr.upazilaid and un.\"UnionId\" = fr.unionid
                                
                                where child_of = 0
                                   
                            order by i.create_date desc");

        $data = array(
            'issues' => $issues
        );

        //var_dump($data);

        return view('issue/issue_view',['data'=>$data]);
    }

    //Updated by Fazlu Bhai
    public function issue_details($id) {


        $comments= DB::select("select * from issues s 
            join users u on u.user_id = s.user_id
            where  s.id = $id and child_of = 0 order by s.created_at desc");

        foreach($comments as $key=>$comment){

            $child= DB::select("select * from issues s 
            join users u on u.user_id = s.user_id
            where  child_of = $comment->id");
            $comments[$key]->child = $child;
        }

        return view('issue/issue_details_view',['comments'=>$comments]);

    }


}