var facility_id = null;
var user_id = 260000;
var comment_id = null;


$(document).ready(function(){
	
	$("#div_dropdown").change(function(){
		
		$("#small-loader").show();
		$('#zilla_group').hide();
		$('#upazila_group').hide();
		
		$.ajax({
		  type: "GET",
		  url: "getzilla",
		  data: {"zillaid":$(this).val()},
		  cache: false,
		  success: function(data){
			 //console.log(data);
			 
			 //Populate Zilla Deopdown
			 
			 $("#zilla_dropdown").empty();
			 $("#upazila_dropdown").empty();
			 
			 $('#zilla_dropdown').append('<option selected="selected"></option>');
			 $.each( data, function( key, val ) {
			  $('#zilla_dropdown').append($('<option>', {value:val.ZillaId, text:val.ZillaNameEng}));
			});
			
			$('#zilla_group').show('slow');

			  //Hide comment panel
			  $("#comment-panel").hide();
			 
			 $("#small-loader").hide();
		  }
		});
		
	});
	
	$("#zilla_dropdown").change(function(){
		
		$("#small-loader").show();
		$('#upazila_group').hide();
		
		$.ajax({
		  type: "GET",
		  url: "getupazila",
		  data: {"zillaid":$(this).val()},
		  cache: false,
		  success: function(data){
			 //console.log(data);
			 
			 //Populate Zilla Deopdown
			 
			 $("#upazila_dropdown").empty();
			 
			 $('#upazila_dropdown').append('<option selected="selected"></option>');
			 $.each( data, function( key, val ) {
				 
					if(val.UpazilaId<10)
						upazilaid = "0" +  val.UpazilaId;
					else
						upazilaid = val.UpazilaId;
					
			  $('#upazila_dropdown').append($('<option>', {value:val.UpazilaId, text:val.UpazilaNameEng}));
			});
			 
			 $("#small-loader").hide();
			  //Hide comment panel
			  $("#comment-panel").hide();
			 $('#upazila_group').show('slow');
		  }
		});
		
	});
	
	$("#upazila_dropdown").change(function(){
		
		$("#small-loader").show();
		//$('#upazila_group').hide();
		//Hide comment panel
		$("#comment-panel").hide();

		$.ajax({
		  type: "GET",
		  url: "getfacility",
		  data: {"zillaid":$("#zilla_dropdown").val(),"upazilaid":$("#upazila_dropdown").val()},
		  cache: false,
		  success: function(data){
			 console.log(data);
			 facility_dropdown(data);



			 return ;
			 
			 $("#facility_table > tbody").empty();
			 
			 $.each(data,function(key, val){
				
				 //console.log(key);
				 
				 cat = val.facility_category==null?"":val.facility_category;
				 
				 
				 if(cat == "A")
					 cat = '<span class="label label-success" style="display:block; padding:3px; width:50px; text-align:center; border-radius:3px;" > A </span>';
				 
				 if(cat == "B")
					 cat = '<span class="label label-warning" style="display:block; padding:3px; width:50px; text-align:center; border-radius:3px;"> B </span>';
				 
				 if(cat == "C")
					 cat = '<span class="label label-danger" style="display:block; padding:3px; width:50px; text-align:center; border-radius:3px;"> C </span>';
				 
				 $('#facility_table tbody').append('<tr data-facilityid='+val.facilityid+' class="facility_tr"><td>'+val.facilityid+'</td><td>'+val.facility_name+'</td><td>'+cat+'</td><td><a href="facility_detail?fid=/'+val.facilityid+'" target="_blank" >Detail</a></td></tr>');
				 
			 });


			 
		  }
		});
		
	});


	//Facility Dropdown

	function facility_dropdown(data){

		$("#facility_dropdown").empty();

		$('#facility_dropdown').append('<option selected="selected"></option>');
		$.each( data, function( key, val ) {

			if (val.UpazilaId < 10)
				upazilaid = "0" + val.UpazilaId;
			else
				upazilaid = val.UpazilaId;

			$('#facility_dropdown').append($('<option>', {value: val.facilityid, text: val.facility_name}));
		});

		$("#small-loader").hide();
	}
	
	
	//Get Comments
	//This will be called when select facilty from faciity dropdown
	
	$(document).on('change','#facility_dropdown',function (){
		
		$("#small-loader").show();
		
		facility_id = $(this).val();
		
		show_comment(facility_id);

		//Set Facility name to detail section
		$("#facility-name").html($("#facility_dropdown option:selected").text());

		$("#small-loader").hide();
		
	});
	
	
	//Add comment NO MORE USE
	/*$('#comments-to-add-btn').click(function(){
		
		
		$.ajax({
		  type: "GET",
		  url: "add_comments",
		  data: {"facility_id":facility_id,"user_id":user_id,"comments":$('#comments-to-add').val()},
		  cache: false,
		  success: function(data){
			 show_comment (facility_id);

			 //Hide comment modal
			  $('#modal-issue').modal('hide');
			 //$('#comments-to-add').val('');
		  }
		});
		
	});*/
	
	
	//Add issue 
	$(document).on('click','#create-issue',function(){
		
		var data_post ={
			facility_id: $("#facility_dropdown").val(),
			user_id : 260000,
			title: 'Title', //$("#issue-title").val(),
			detail: $("#issue-detail").val(),
			category: $("#issue-category").val(),
			priority: $("#issue-priority").val(),
			tags:$("#tags").val(),
			create_date: $("#issue-create-date").val(),
			completion_date:$("#issue-completion-date").val(),
			resolved:''
			
		};

		//show loader
		$(".app-loader").show();
		$.ajax({
		  type: "GET",
		  url: "add_comments",
		  data: data_post,
		  cache: false,
		  success: function(data){
			 show_comment (facility_id);
			 //$('#comments-to-add').val('');
			  $("#modal-issue").modal('hide');
			  $(".app-loader").hide();//hide loader
		  }
		});
		
	});
	
});

//Show facility information by ID
function show_comment (facility_id) {

	$(".app-loader").show();

	$.ajax({
		type: "GET",
		url: "get_comments",
		data: {"facility_id": facility_id},
		cache: false,
		success: function (data) {

			console.log(data);

			$("#small-loader").hide();
			msg = "<div class=\"direct-chat-messages\" style=\"height: auto; padding-right: 0px; padding-left: 0px;\">";

			var cls, status_btn, imgs, imgtitle,reply_comments = null;

			$.each(data, function (key, val) {

				reply_comments ='';
				var reply = '';


				if (val.category == "issue") {
					//cls = 'bg-red';


					if (val.status == "resolved") {
						imgs = 'public/image/issue-completed.png';
						imgtitle = 'Issue Resolved';
						status_btn = "";
					} else if (val.status == "postpone") {
						imgs = 'public/image/issue-postpone.png';
						imgtitle = 'Issue Postpone';
						status_btn = "";
					} else {
						imgs = 'public/image/issue.png';
						imgtitle = 'Issue';
						status_btn = "&nbsp;<button title='Status' class=\"btn btn-success btn-xs issue-status\" data-issueid=\"" + val.id + "\" " +
							" data-issuestatus=\"" + val.status + "\" type=\"button\">" +
							"<span class=\"fa fa-flag-checkered\"></span></button>";
					}

					if (val.resolved != "" && val.resolved != null) {
						reply = '<div style="background-color: #FFF"><span class="fa fa-reply"></span> ' + val.resolved + '</div>';
					} else
						reply = '';

				} else {
					//cls = 'bg-aqua';
					status_btn = "";
					imgs = 'public/image/comment.png';
					imgtitle = 'Comments';
				}

				//reply info
				$.each(val.child, function (key1, val1){
					reply_comments += '<div style="padding: 5px; margin-bottom: 3px background-color: #dddddd; border-bottom: solid 1px #ccc;">'+val1.details
						+ '<br><small><i class="fa fa-user-circle"></i> '+ val1.name+' <i class="fa fa-calendar"></i> '+formatted_date(val1.created_at)+'</small>' +
						'</div>';
				});

				msg +=
					'<!-- Message. Default to the left -->' +
					'<div class="box"><div class="box-body">'+
					'<div class="direct-chat-msg" >' +
					'<div class="direct-chat-info clearfix">' +
					'<span class="direct-chat-name pull-left"><i class="fa fa-user"></i> ' + val.name + '</span> &nbsp;&nbsp;' +
					'<span class="direct-chat-timestamp" style="color:#000;"><i class="fa fa-calendar"></i> ' + formatted_date(val.created_at) + '</span>' +
					'</div>' +
					'<!-- /.direct-chat-info -->' +
					'<img class="direct-chat-img" title="' + imgtitle + '" src="' + imgs + '">' +
					'<!-- /.direct-chat-img -->' +
					'<div class="direct-chat-text ' + cls + '" >' + val.details + '' +
					reply +
					'</div>' +

					'<div class="direct-chat-btn">' +
					//'<button class="btn btn-info btn-xs" type="button" class="comments-reply"><span class="fa fa-reply"></span></button>' +
					//Reply comment
					'<button title="Reply" class="btn btn-xs reply-issue" data-parentid='+val.id+'><span class="fa fa-reply"></span></button>' +
					status_btn +

					'<hr>'+reply_comments+''+

					'</div>' +
					'</div>' +
					'<!-- /.direct-chat-text -->'+
					'</div></div>'
				;


				reply = '';
			});

			msg += '</div><!-- /.direct-chat-msg -->';


			$("#comment-body").html(msg);

			//Show comment panel
			$("#comment-panel").show();

			$(".app-loader").hide();//hide loader

		}
	});
}

	//Modal issue status change
	//This will fire when, user click on issue status change button (Flag)
	$(document).on('click','.issue-status',function (){
		comment_id = $(this).data('issueid');
		issuestatus = $(this).data('issuestatus');

		$("#issue-status").val(issuestatus);
		$("#modal-issue-status").modal('show');
	});

	$(document).on('click','#change-status',function (){

		facility_id = $("#facility_dropdown").val();

		$(".app-loader").show();//show loader

		var data_post ={
			facility_id: facility_id,
			comment_id : comment_id,
			status: $("#issue-status").val(),
			resolved: $("#issue-update-detail").val()
		};

		//$(".app-loader").show();//show loader

		$.ajax({
			type: "GET",
			url: "update_status",
			data: data_post,
			cache: false,
			success: function(data){
				show_comment (facility_id);
				//$('#comments-to-add').val('');

				$(".app-loader").hide();//hide loader
			}
		});

		$("#modal-issue-status").modal('hide');
	});


	//Reset Password
	$(document).on('click','#reset_submit',function (){

		//Get Field values
		var oldpass = $("#oldpass").val();
		var newpass = $("#newpass").val();
		var repass = $("#repass").val();

		//Check Password validity
		if(oldpass.length<2 || newpass.length<6 || repass != newpass)
		{
			if(oldpass.length<2)
			{
				sweetAlert('Please enter old pass');
				return false;
			}

			if(newpass.length<2){
				sweetAlert('Password should be minimum 2 characters long');
				return false;
			}

			if(repass != newpass){
				sweetAlert('New pass and Re-type password dose not match');
				return false;
			}

		}

		//Generating JSON for POST
		var data_post ={
			pass: $("#newpass").val(),
			oldpass: $("#oldpass").val()
		};

		//Show loader
		$(".app-loader").show();

		//Jquery Execution started
		$.ajax({
			type: "GET",
			url: "password_change",
			data: data_post,
			cache: false,
			success: function(data){
				sweetAlert(data);

				//Clear Field
				$("#oldpass").val('');
				$("#newpass").val('');
				$("#repass").val('');

				//hide loader
				$(".app-loader").hide();
			}
		});

	});


	//reply comment
//Open Reply Modal
$(document).on('click',".reply-issue",function(){
	$("#issue-reply-txt").val('');
	$("#reply_parent_id").val($(this).data('parentid'));
	$('#modal-reply').modal('show');
});

$(document).on('click',"#reply-btn",function(){




	var data_post ={
		facility_id: $("#facility_dropdown").val(),
		title: 'Reply', //$("#issue-title").val(),
		detail: $("#issue-reply-txt").val(),
		category: 'comment',
		priority: 'normal',
		tags:'',
		create_date: current_date(),
		completion_date:current_date(),
		child_of: $("#reply_parent_id").val()

	};

	//show loader
	$(".app-loader").show();
	$.ajax({
		type: "GET",
		url: "add_comments",
		data: data_post,
		cache: false,
		success: function(data){
			show_comment (facility_id);

			$("#reply_parent_id").val('');
			$('#modal-reply').modal('hide');

			$(".app-loader").hide();//hide loader
		}
	});





});


function current_date(){
	formattedDate = new Date();
	d = formattedDate.getDate();
	m =  formattedDate.getMonth() + 1; // JavaScript months are 0-11

	h = formattedDate.getHours();
	i = formattedDate.getMinutes();
	s = formattedDate.getSeconds();

	y = formattedDate.getFullYear();



	return y + "-" + m + "-" + d +' '+ h +":" + i +":" + s;
}

function formatted_date(date_time){
	 formattedDate = new Date(date_time);
	 d = formattedDate.getDate();
	 m =  formattedDate.getMonth() + 1; // JavaScript months are 0-11

	 h = formattedDate.getHours();
	 i = formattedDate.getMinutes();
	 s = formattedDate.getSeconds();

	 y = formattedDate.getFullYear();



	return d + "-" + m + "-" + y +' &nbsp;<i class="fa fa-clock-o"></i> '+ h +":" + i ;
}

$(document).ready( function () {
	$('.datatable').DataTable();

	$(".clickable-row").click(function() {

		window.location = $(this).data("href");
	});
} );



