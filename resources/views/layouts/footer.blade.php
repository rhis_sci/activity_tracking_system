<!-- Main Footer -->
<footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
        <strong>Version 1.0</strong>
    </div>
    <!-- Default to the left -->
    <strong>Copyright © 2018 <a href="#" target="_blank">eMIS</a>. All rights reserved.</strong>
</footer>