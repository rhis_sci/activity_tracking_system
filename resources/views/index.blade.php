<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Activity Tracking</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" >
    <link href="{{ asset('public/css/bootstrap.min.css') }}" rel="stylesheet">

<!-- Font Awesome -->
    <link rel="stylesheet" href="{{ asset("public/font-awesome/css/font-awesome.min.css") }}">
<!-- Theme style -->
    <link rel="stylesheet" href="{{ asset("public/AdminLTE/dist/css/skins/skin-purple.min.css") }}">

    <link rel="stylesheet" href="{{ asset("public/AdminLTE/plugins/iCheck/all.css") }}">

    <link rel="stylesheet" href="{{ asset("public/AdminLTE/plugins/select2/select2.min.css") }}">
    <link rel="stylesheet" href="{{ asset("public/AdminLTE/nprogress/nprogress.css") }}">
    <link rel="stylesheet" href="{{ asset("public/AdminLTE/sweetalert/dist/sweetalert.css") }}">
    <link rel="stylesheet" href="{{ asset("public/AdminLTE/nestable/nestable.css") }}">
    <link rel="stylesheet" href="{{ asset("public/AdminLTE/toastr/build/toastr.min.css") }}">
    <link rel="stylesheet" href="{{ asset("public/AdminLTE/bootstrap3-editable/css/bootstrap-editable.css") }}">
    <link rel="stylesheet" href="{{ asset("public/AdminLTE/dist/css/AdminLTE.min.css") }}">
    <link rel="stylesheet" href="{{ asset("public/AdminLTE/jqgrid/jqgrid.min.css") }}">
    <link rel="stylesheet" href="{{ asset("public/AdminLTE/jquery-ui/jquery-ui.css")}}">
    <link rel="stylesheet" href="{{ asset("public/css/common.css") }}" type="text/css">
    <link rel="stylesheet" href="{{ asset("public/css/tags-input-with-autocomplete_typeahead.css") }}" type="text/css">
    <link rel="stylesheet" href="{{ asset("public/css/tags-input-with-autocomplete_bootstrap-tagsinput.css") }}" type="text/css">
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css" type="text/css">



    {{--<!-- REQUIRED JS SCRIPTS -->--}}
    <script src="{{ asset("public/js/popper.min.js")}}" type="application/javascript"></script>
    <script src="{{ asset("public/js/jquery.min.js")}}" type="application/javascript"></script>
    <script src="{{ asset('public/js/bootstrap.min.js') }}" type="application/javascript"> </script>
    <script src="{{ asset ("public/AdminLTE/plugins/slimScroll/jquery.slimscroll.min.js") }}"></script>
    <script src="{{ asset ("public/AdminLTE/dist/js/app.min.js") }}" type="application/javascript"></script>
    <script src="{{ asset ("public/AdminLTE/jquery-pjax/jquery.pjax.js") }}" type="application/javascript"></script>
    <script src="{{ asset ("public/AdminLTE/nprogress/nprogress.js") }}" type="application/javascript"></script>
    <script src="{{ asset ("public/AdminLTE/jqgrid/jqgrid.min.js") }}" type="application/javascript"></script>
    <script src="{{ asset ("public/AdminLTE/plugins/bootbox/bootbox.js") }}" type="application/javascript"></script>
    <script src="{{ asset ("public/js/tags-input-with-autocomplete_typeahead.js") }}" type="application/javascript"></script>
    <script src="{{ asset ("public/js/tags-input-with-autocomplete_bootstrap-tagsinput.js") }}" type="application/javascript"></script>
    <script src="//cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js" type="application/javascript"></script>

</head>

<body class="hold-transition skin-purple sidebar-mini">
<div class="app-loader" style="position: absolute;
    width: 100%;
    height: 100%;
    z-index: 10000;
    opacity: 1;
    display: none;">
    <div class="loader-back"></div>
    <div style="position: absolute;left: 45%;top: 40%;z-index: 10001;"><img src="{{asset('public/image/loading-blue.gif')}}" width="50%"> </div>
</div>
<div class="wrapper">
    @include('layouts.header')
    @include('layouts.sidebar')
    <div class="content-wrapper" id="pjax-container">
        @yield('content')
        {{--{!! Admin::script() !!}--}}
    </div>
    @include('layouts.footer')
</div>

<!-- ./wrapper -->

@yield('script')
<script>
    function LA() {}
    LA.token = "{{ csrf_token() }}";
    $(function () {
        $('input').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
            increaseArea: '20%' // optional
        });
    });
</script>

<!-- REQUIRED JS SCRIPTS -->

<script src="{{ asset ("public/AdminLTE/jquery-ui/jquery-ui.js") }}"></script>
<script src="{{ asset ("public/AdminLTE/nestable/jquery.nestable.js") }}"></script>
<script src="{{ asset ("public/AdminLTE/toastr/build/toastr.min.js") }}"></script>
<script src="{{ asset ("public/AdminLTE/bootstrap3-editable/js/bootstrap-editable.min.js") }}"></script>
<script src="{{ asset ("public/AdminLTE/sweetalert/dist/sweetalert.min.js") }}"></script>
<script src="{{ asset ("public/AdminLTE/plugins/select2/select2.full.min.js") }}"></script>
<script src="{{ asset("public/AdminLTE/plugins/iCheck/icheck.min.js")}}"></script>

{{--{!! Admin::js() !!}--}}
<script src="{{ asset ("public/js/admin_theme.js") }}"></script>
<script src="{{ asset("public/js/jquery-serialize.js")}}" type="application/javascript"></script>
<script src="{{ asset("public/js/common.js")}}" type="application/javascript"></script>
<script src="{{ asset ("public/js/comment.js") }}"></script>




</body>
</html>

